#!/bin/bash
ENVARS_FILE=.envars.conf

# Create our client specific environmental variables
touch $HOME/$ENVARS_FILE

######################################
####   Application installation   ####
######################################

# Install Oh my zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Install neovim plug
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

######################################
#### Application specific configs ####
######################################

# ZSH Config
ln -s $PWD/zshrc $HOME/.zshrc

# Waybar config
mkdir -p $HOME/.config/waybar
ln -s $PWD/waybar_config $HOME/.config/waybar/config

# Sway config
mkdir -p $HOME/.config/sway
ln -s $PWD/sway_config $HOME/.config/sway/config

# Termite config
mkdir -p $HOME/.config/termite
ln -s $PWD/termite_config $HOME/.config/termite/config

# Neovim config
mkdir -p $HOME/.config/nvim
ln -s $PWD/nvim_config $HOME/.config/nvim/init.vim

######################################
####  Linking services / timers   ####
######################################

# Create user service directory
mkdir -p $HOME/.config/systemd/user

# Create user service environment directory
mkdir -p $HOME/.config/environment.d

# Create file for environmental variables for services 
# This is where you need to set Nextcloud app passwords/username, etc
touch $HOME/.config/environment.d/envars.conf

# Link nextcloud timer and service
ln -s $PWD/services/nextcloud.timer $HOME/.config/systemd/user/nextcloud.timer
ln -s $PWD/services/nextcloud.service $HOME/.config/systemd/user/nextcloud.service

## Enable and start our services / timers
systemctl --user enable nextcloud.timer
systemctl --user start nextcloud.timer
